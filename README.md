__trip__

trip is a concurrent tracer for the ruby programming language. it yields control
between two threads, most likely the main thread and a thread that trip creates.

__notes__

trip is implemented with "Thread#set_trace_func". it can be a big bottleneck, 
even more so when there are a large number of events being created. i have seen
ruby grind to a halt when the codepath being traced is busy. it wouldn't be a good
library to use with a production rails application, but it could be useful for 
building developer tools outside of a production environment.

__examples__

the "examples/" directory includes additional examples.

__1.__

the code being traced is paused on method call and method return events
by default.

```ruby
def add(x,y)
  x + y
end

trip = Trip.new { add(20,50) }
e1 = trip.start  # Trip::Event  (for the method call of "#add")
e2 = trip.resume # Trip::Event  (for the method return of "#add")
e3 = trip.resume # returns nil (thread exits)
```

__2.__

a predicate Proc can be implemented as a custom value. it receives
an instance of `Trip::Event` that can support the Proc when it is
making a decision to return a true(pause) or false(continue) value.

```ruby
class Planet
  def initialize(name)
    @name = name
  end

  def echo
    'ping'
  end
end

trip = Trip.new { Planet.new('earth').echo }
trip.pause? { |e| # pause on a method call
  e.name == 'call'
}
e1 = trip.start   # Trip::Event (for the method call of `Planet#initialize`)
e2 = trip.resume  # Trip::Event (for the method call of `Planet#echo`)
e3 = trip.resume  # returns nil (thread exits)
```

__3.__

Trip::Event#binding can be used to evaluate ruby while the trip 
thread is sleeping. the example returns the value of the local 
variable "x".


```ruby
def add(x,y)
  x + y
end

trip = Trip.new { add(2,3) }
e1 = trip.start           # Trip::Event (for the method call of add)
x  = e1.binding.eval('x') # returns 2   (x is equal to 2)
trip.stop                 # thread exits

puts "x is equal to #{x}"
```

__dev__

	# install dependencies
	[sudo] gem install bundler
	bundle install --path=.gems

	# run the tests
	ruby -S rake

	# use the library
	ruby -Ilib myscript.rb

__ruby__

- MRI / YARV / CRuby.
  1.9+, 2.0+
