$LOAD_PATH << 'lib'
require 'trip'
Gem::Specification.new do |g|
  g.name        = 'trip'
  g.version     = Trip::VERSION
  g.summary     = 'concurrent ruby tracer'
  g.description = 'concurrent ruby tracer built with Thread#set_trace_func'
  g.files	= `git ls-files`
end